package org.torproject.metrics.bot.microblog;

import java.awt.image.RenderedImage;
import java.util.List;

public interface MicroblogAccount {

  /**
   * Updates the status for the microblogging account. This method should be
   * implemented for each component. Implementations should attempt to recover
   * from temporary errors, such as timeouts on HTTP connections.
   * 
   * @param text
   *          The text of a status update
   * @param image
   *          An image to be included in the status update, or null if no image
   *          is included
   * @throws MicroblogUpdateFailedException
   *           Thrown if a status update fails for a microblog.
   */
  void updateStatus(String text, RenderedImage image)
          throws MicroblogUpdateFailedException;

  /**
   * Conditionally updates the status for the microblogging account. This method
   * is implemented by {@link BaseMicroblogAccountImpl} so classes extending
   * that class would only need to implement
   * {@link #updateStatus(String, RenderedImage)}. This method should check to
   * see that the status update has a type that is desired by the particular
   * microblogging account before updating the status.
   * 
   * @param status
   *          The generated status update
   * @throws MicroblogUpdateFailedException
   *           Thrown if a status update fails for a microblog.
   */
  void updateStatus(MicroblogStatus status)
          throws MicroblogUpdateFailedException;

  /**
   * Gets the list of desirable status update types for this account.
   * 
   * @return The list of desirable status update types.
   * @see StatusType
   */
  List<StatusType> getStatusTypes();

  String getFriendlyName();
}