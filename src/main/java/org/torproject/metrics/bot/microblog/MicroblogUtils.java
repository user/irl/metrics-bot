package org.torproject.metrics.bot.microblog;

import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.time.temporal.Temporal;

/**
 * Utility functions useful for formatting Tweets.
 *
 * @author Iain R. Learmonth &lt;irl@torproject.org&gt;
 */
public class MicroblogUtils {
  /**
   * Takes a {@link Temporal} and calculates the time since then, and returns an
   * English-speaking human readable representation. As the delta magnitude
   * increases, accuracy decreases.
   *
   * <p>Output is either in years and days, days and hours, hours and minues or
   * minutes and seconds.
   */
  public static String formatAge(Temporal firstSeenDate) {
    int years = (int) ChronoUnit.YEARS.between(firstSeenDate,
            ZonedDateTime.now(ZoneOffset.UTC));
    int days = (int) ChronoUnit.DAYS.between(firstSeenDate,
            ZonedDateTime.now(ZoneOffset.UTC));
    int hours = (int) ChronoUnit.HOURS.between(firstSeenDate,
            ZonedDateTime.now(ZoneOffset.UTC));
    int minutes = (int) ChronoUnit.MINUTES.between(firstSeenDate,
            ZonedDateTime.now(ZoneOffset.UTC));
    int seconds = (int) ChronoUnit.SECONDS.between(firstSeenDate,
            ZonedDateTime.now(ZoneOffset.UTC));

    if (years > 0) {
      return String.format("%s years and %s days", years, (days % 365));
    } else if (days > 0) {
      return String.format("%s days and %s hours", days, (hours % 24));
    } else if (hours > 0) {
      return String.format("%s hours and %s minutes", hours, (minutes % 60));
    } else if (minutes > 0) {
      return String.format("%s minutes and %s seconds", minutes,
              (seconds % 60));
    } else {
      return String.format("%s seconds", seconds);
    }
  }

  /**
   * Returns a human-readable representation of a bandwidth.
   * The output uses binary SI units.
   *
   * @param bandwidth Information rate in bytes per second
   */
  public static String formatBandwidth(Long bandwidth) {
    Double bandwidthDouble = bandwidth.doubleValue();
    String[] units = { "", "Ki", "Mi", "Gi", "Ti", "Pi", "Ei", "Zi" };
    for (String unit : units) {
      if (Math.abs(bandwidthDouble) < 1024.0) {
        return String.format("%3.1f %sB/s", bandwidthDouble, unit);
      }
      bandwidthDouble /= 1024.0;
    }
    return String.format("%.1f %sB/s", bandwidthDouble, "Yi");
  }
}