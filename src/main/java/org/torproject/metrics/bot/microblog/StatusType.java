package org.torproject.metrics.bot.microblog;

public enum StatusType {

  /* Regarding a specific relay */
  RELAY,

  /* Regarding the Tor network */
  NETWORK,

  /* Regarding an alert with a Metrics Team service */
  ALERT,

  /* Regarding an alert with the Tor Network */
  EVENT;

}
