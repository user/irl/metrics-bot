package org.torproject.metrics.bot.microblog;

public class MicroblogStatusTooLongException
        extends MicroblogUpdateFailedException {

  private static final long serialVersionUID = 1L;

  private final int maxLongness;

  public MicroblogStatusTooLongException(int maxLongness) {
    this.maxLongness = maxLongness;
  }

  public int getMaxLongness() {
    return maxLongness;
  }

}
