package org.torproject.metrics.bot.twitter;

import org.torproject.metrics.bot.MetricsBot;
import org.torproject.metrics.bot.microblog.BaseMicroblogAccountImpl;
import org.torproject.metrics.bot.microblog.MicroblogAccount;
import org.torproject.metrics.bot.microblog.MicroblogStatusTooLongException;
import org.torproject.metrics.bot.microblog.MicroblogUpdateFailedException;

import twitter4j.Status;
import twitter4j.StatusUpdate;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.conf.ConfigurationBuilder;

import java.awt.image.RenderedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.imageio.ImageIO;

/**
 * Microblogging interface to a Twitter account.
 *
 * @author Iain R. Learmonth &lt;irl@torproject.org&gt;
 */
public class TwitterAccount extends BaseMicroblogAccountImpl
        implements MicroblogAccount {

  private String friendlyName;
  private TwitterFactory twitterFactory;

  /**
   * Creates a new Twitter account instance using OAuth credentials as specified
   * in the configuration.
   *
   * @param configuration
   *          The configuration specific to the Twitter account.
   */
  public TwitterAccount(TwitterAccountConfiguration configuration) {
    super(configuration);
    friendlyName = configuration.getFriendlyName();
    ConfigurationBuilder cb = new ConfigurationBuilder();
    cb.setOAuthConsumerKey(configuration.getConsumerKey())
            .setOAuthConsumerSecret(configuration.getConsumerSecret())
            .setOAuthAccessToken(configuration.getToken())
            .setOAuthAccessTokenSecret(configuration.getTokenSecret());
    twitterFactory = new TwitterFactory(cb.build());
  }

  @Override
  public void updateStatus(String text, RenderedImage image)
          throws MicroblogUpdateFailedException {
    List<String> urls = extractUrls(text);
    int urlsLength = urls.stream().mapToInt(i -> i.length()).sum();
    int textLength = text.length() - urlsLength + (22 * urls.size());

    if (textLength > 280) {
      throw new MicroblogStatusTooLongException(280);
    }

    StatusUpdate update = new StatusUpdate(text);

    if (image != null) {
      try {
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        ImageIO.write(image, "png", os);
        InputStream fis = new ByteArrayInputStream(os.toByteArray());
        update.setMedia(
                String.format("image%d.png", System.currentTimeMillis()), fis);
      } catch (IOException e) {
        MetricsBot.error("Failed to handle image for a status update "
                + "for Twitter account: " + friendlyName);
      }
    }

    int attempt = 0;
    while (true) {
      try {
        Status status = twitterFactory.getInstance().updateStatus(update);
        System.out.println("Successfully updated the status for " + friendlyName
                + " to [" + status.getText() + "].");
        break;
      } catch (TwitterException e) {
        if (attempt++ == 3) {
          MetricsBot.error(
                  "Failed to perform a status update for Twitter account "
                          + "after 3 attempts: " + friendlyName);
          throw new MicroblogUpdateFailedException();
        }
      }
    }
  }

  private static List<String> extractUrls(String text) {
    List<String> containedUrls = new ArrayList<String>();
    Pattern urlPattern = Pattern.compile(
            "(?:^|[\\W])((ht|f)tp(s?):\\/\\/|www\\.)"
                    + "(([\\w\\-]+\\.){1,}?([\\w\\-.~]+\\/?)*"
                    + "[\\p{Alnum}.,%_=?&#\\-+()\\[\\]\\*$~@!:/{};']*)",
            Pattern.CASE_INSENSITIVE | Pattern.MULTILINE | Pattern.DOTALL);
    Matcher urlMatcher = urlPattern.matcher(text);

    while (urlMatcher.find()) {
      containedUrls.add(text.substring(urlMatcher.start(0), urlMatcher.end(0)));
    }

    return containedUrls;
  }

  @Override
  public String getFriendlyName() {
    return friendlyName;
  }
}
