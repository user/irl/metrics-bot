package org.torproject.metrics.bot.factoids;

import org.apache.commons.configuration.ConfigurationException;
import org.apache.commons.configuration.HierarchicalConfiguration;
import org.apache.commons.configuration.XMLConfiguration;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Contains the configuration for factoids as loaded from the XML
 * configuration. The XML configuration is included in the source
 * for metrics-bot at <code>src/main/resources/factoids.xml</code>.
 *
 * <p>Currently only the definition of static factoids is supported
 * in the configuration file. These consist of one or more trigger
 * words and a single static response.
 *
 * <p>An example <code>factoids.xml</code> is shown below:
 *
 * <pre>
 * &lt;factoids&gt;
 *  &lt;factoid&gt;
 *   &lt;trigger&gt;ask&lt;/trigger&gt;
 *   &lt;response&gt;Please don't ask to ask your question, just ask.
 *    It's useful to include the version of Tor Browser or tor that you're
 *    using and the operating system.&lt;/response&gt;
 *  &lt;/factoid&gt;
 * &lt;/factoids&gt;
 * </pre>
 *
 * @author Iain R. Learmonth &lt;irl@torproject.org&gt;
 * @see org.torproject.metrics.bot.factoids.StaticFactoid
 */
public class FactoidConfiguration {

  private static FactoidConfiguration factoidConfiguration;

  private XMLConfiguration xmlConfiguration;

  /**
   * Creates a new FactoidConfiguration.
   *
   * @throws FileNotFoundException if factoids.xml cannot be found
   * @throws IOException if factoids.xml cannot be read
   * @throws ConfigurationException if factoids.xml is not well formed
   */
  private FactoidConfiguration()
          throws FileNotFoundException, IOException, ConfigurationException {
    URL factoidsUrl =
            this.getClass().getClassLoader().getResource("factoids.xml");

    xmlConfiguration = new XMLConfiguration();
    xmlConfiguration.setDelimiterParsingDisabled(true);
    xmlConfiguration.load(factoidsUrl);
  }

  /**
   * Returns a list of factoids as loaded from the XML configuration.
   *
   * @return a list of factoids as loaded from the XML configuration
   */
  public List<Factoid> getFactoids() {
    List<Factoid> factoids = new ArrayList<Factoid>();
    List<HierarchicalConfiguration> fields =
            xmlConfiguration.configurationsAt("factoid");
    for (HierarchicalConfiguration sub : fields) {
      List<String> triggers = new ArrayList<String>();
      Collections.addAll(triggers, sub.getStringArray("trigger"));
      Factoid factoid = new StaticFactoid(triggers, sub.getString("response"));
      factoids.add(factoid);
    }
    
    // TODO: Load these from the XML file instead
    factoids.add(new NetworkSizeFactoid());
    factoids.add(new CountryFactoid());
    factoids.add(new AutonomousSystemFactoid());
    return factoids;
  }

  /**
   * Get the singleton FactoidConfiguration object.
   *
   * @return the singleton FactoidConfiguration object
   */
  public static FactoidConfiguration getSingleton() {
    if (factoidConfiguration == null) {
      try {
        factoidConfiguration = new FactoidConfiguration();
      } catch (IOException | ConfigurationException e) {
        // who would I be without factoids?
        e.printStackTrace();
        System.exit(1);
      }
    }

    return factoidConfiguration;
  }

}
