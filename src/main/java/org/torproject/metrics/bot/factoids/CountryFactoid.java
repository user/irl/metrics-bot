package org.torproject.metrics.bot.factoids;

import org.torproject.metrics.bot.MetricsBot;
import org.torproject.metrics.bot.microblog.RelayLocationStatusGenerator;
import org.torproject.metrics.bot.tor.OnionooMissingInformationException;
import org.torproject.metrics.bot.tor.Relay;

import java.util.List;

public class CountryFactoid implements Factoid {

  @Override
  public Boolean isTriggered(String trigger) {
    return trigger.equals("country");
  }

  @Override
  public String getFactoid(String lookup) {
    if (lookup.length() != 2) {
      return "I can only identify countries by their ISO 3166-2 alpha-2 "
              + "code. See: https://en.wikipedia.org/wiki/"
              + "ISO_3166-1_alpha-2#Decoding_table. For example:"
              + " ?country de";
    }
    List<Relay> relays =
            MetricsBot.getTorNetwork()
                    .getRunningRelaysForCountry(lookup.toLowerCase());
    try {
      return RelayLocationStatusGenerator.generateCountryBandwidthTweet(relays)
              .getText();
    } catch (IndexOutOfBoundsException e) {
      return "I don't know that country, either it has no relays or it's not"
              + " a valid ISO 3166-1 alpha-2 code. Sorry.";
    } catch (OnionooMissingInformationException e) {
      return "Something went wrong. Sorry.";
    }
  }

}
