package org.torproject.metrics.bot.tor;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.time.ZonedDateTime;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

class GsonRelay extends GsonBaseRelay implements Relay {
  String[] exitAddresses;
  String dirAddress;
  ZonedDateTime lastChangedAddressOrPort;
  Boolean hibernating;
  String country;
  String countryName;
  String regionName;
  String cityName;
  Double latitude;
  Double longitude;
  String asNumber;
  String asName;
  Long consensusWeight;
  String hostName;
  Long bandwidthRate;
  Long bandwidthBurst;
  Long observedBandwidth;
  String[] exitPolicy;
  GsonExitPolicySummary exitPolicySummary;
  GsonExitPolicySummary exitPolicyV6Summary;
  String contact;
  Boolean recommendedVersion;
  String[] effectiveFamily;
  String[] allegedFamily;
  String[] indirectFamily;
  Double consensusWeightFraction;
  Double guardProbability;
  Double middleProbability;
  Double exitProbability;
  Boolean measured;

  @Override
  public List<String> getExitAddresses() {
    return Arrays.asList(exitAddresses);
  }

  @Override
  public String getDirAddress() {
    return dirAddress;
  }

  @Override
  public ZonedDateTime getLastChangedAddressOrPort() {
    return lastChangedAddressOrPort;
  }

  @Override
  public Boolean isHibernating() {
    return hibernating;
  }

  @Override
  public String getCountry() {
    return country;
  }

  @Override
  public String getCountryName() {
    return countryName;
  }

  @Override
  public String getRegionName() {
    return regionName;
  }

  @Override
  public String getCityName() {
    return cityName;
  }

  @Override
  public Double getLatitude() {
    return latitude;
  }

  @Override
  public Double getLongitude() {
    return longitude;
  }

  @Override
  public String getAsNumber() {
    return asNumber;
  }

  @Override
  public String getAsName() {
    return asName;
  }

  @Override
  public Long getConsensusWeight() {
    return consensusWeight;
  }

  @Override
  public String getHostName() {
    return hostName;
  }

  @Override
  public Long getBandwidthRate() {
    return bandwidthRate;
  }

  @Override
  public Long getBandwidthBurst() {
    return bandwidthBurst;
  }

  @Override
  public Long getObservedBandwidth() {
    return observedBandwidth;
  }

  @Override
  public List<String> getExitPolicy() {
    return Arrays.asList(exitPolicy);
  }

  @Override
  public Map<String, String> getExitPolicySummary() {
    return exitPolicySummary.toSimpleMap();
  }

  @Override
  public Map<String, String> getExitPolicyV6Summary() {
    return exitPolicyV6Summary.toSimpleMap();
  }

  @Override
  public String getContact() {
    return contact;
  }

  @Override
  public Boolean isRecommendedVersion() {
    return recommendedVersion;
  }

  @Override
  public List<String> getEffectiveFamily() {
    return Arrays.asList(effectiveFamily);
  }

  @Override
  public List<String> getAllegedFamily() {
    return Arrays.asList(allegedFamily);
  }

  @Override
  public List<String> getIndirectFamily() {
    return Arrays.asList(indirectFamily);
  }

  @Override
  public Double getConsensusWeightFraction() {
    return consensusWeightFraction;
  }

  @Override
  public Double getGuardProbability() {
    return guardProbability;
  }

  @Override
  public Double getMiddleProbability() {
    return middleProbability;
  }

  @Override
  public Double getExitProbability() {
    return exitProbability;
  }

  @Override
  public Boolean getMeasured() {
    return measured;
  }

  @Override
  public String humanString(Boolean withRelaySearchUrl) {
    String flags = "";
    for (RelayFlag flag : getFlags()) {
      flags += flag.getHumanName() + " ";
    }
    String humanString =
            getNickname()
                    + " - CW: " + getConsensusWeight() + " [ " + flags + "]";
    if (withRelaySearchUrl) {
      humanString +=
              " https://metrics.torproject.org/rs.html#details/"
                      + getFingerprint();
    }
    return humanString;
  }

  @Override
  public String toString() {
    return "<GsonRelay nickname="
            + getNickname() + ", fingerprint=" + getFingerprint()
            + ", orAddresses=" + getOrAddresses().toString() + ", lastSeen="
            + getLastSeen() + ", firstSeen=" + getFirstSeen() + ", running="
            + isRunning() + ", flags=" + getFlagStrings().toString()
            + ", lastRestarted=" + getLastRestarted() + ", advertisedBandwidth="
            + getAdvertisedBandwidth() + ", platform=" + getPlatform()
            + ", exitAddresses=" + Arrays.toString(exitAddresses)
            + ", dirAddress=" + dirAddress + ", lastChangedAddressOrPort="
            + lastChangedAddressOrPort + ", hibernating=" + hibernating
            + ", country=" + country + ", countryName=" + countryName
            + ", regionName=" + regionName + ", cityName=" + cityName
            + ", latitude=" + latitude + ", longitude=" + longitude
            + ", asNumber=" + asNumber + ", asName=" + asName
            + ", consensusWeight=" + consensusWeight + ", hostName=" + hostName
            + ", bandwidthRate=" + bandwidthRate + ", bandwidthBurst="
            + bandwidthBurst + ", observedBandwidth=" + observedBandwidth
            + ", exitPolicy=" + Arrays.toString(exitPolicy)
            + ", exitPolicySummary=" + exitPolicySummary
            + ", exitPolicyV6Summary=" + exitPolicyV6Summary + ", contact="
            + contact + ", recommendedVersion=" + recommendedVersion
            + ", effectiveFamily=" + Arrays.toString(effectiveFamily)
            + ", allegedFamily=" + Arrays.toString(allegedFamily)
            + ", indirectFamily=" + Arrays.toString(indirectFamily)
            + ", consensusWeightFraction=" + consensusWeightFraction
            + ", guardProbability=" + guardProbability + ", middleProbability="
            + middleProbability + ", exitProbability=" + exitProbability
            + ", measured=" + measured + ">";
  }

  @Override
  protected void drawBadgeSpecifics(Graphics2D g2) {
    drawBadgeProbabilities(g2);
  }

  private void drawBadgeProbabilities(Graphics2D g2) {
    // Draw headings
    g2.setColor(new Color(125, 70, 152));
    g2.setFont(new Font("Source Sans Pro", Font.BOLD, 12));
    g2.drawString("Guard Probability".toUpperCase(), 45, 260);
    g2.drawString("Middle Probability".toUpperCase(), 215, 260);
    g2.drawString("Exit Probability".toUpperCase(), 385, 260);

    // Draw values
    g2.setColor(new Color(255, 255, 255));
    g2.setFont(new Font("Source Sans Pro Light", Font.PLAIN, 26));
    g2.drawString(String.format("%.5f", getGuardProbability()), 45, 290);
    g2.drawString(String.format("%.5f", getMiddleProbability()), 215, 290);
    g2.drawString(String.format("%.5f", getExitProbability()), 385, 290);
  }
}