package org.torproject.metrics.bot.tor;

import org.torproject.metrics.bot.microblog.MicroblogUtils;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.io.IOException;
import java.util.List;

import javax.imageio.ImageIO;

public class CountryImpl extends RelayAggregateImpl implements Country {

  public CountryImpl(List<Relay> relays) {
    this.relays = relays;
  }

  @Override
  public String getCountry() {
    return relays.get(0).getCountry();
  }

  @Override
  public String getCountryName() {
    return relays.get(0).getCountryName();
  }

  @Override
  public RenderedImage generateBadge() {
    BufferedImage badge = new BufferedImage(512, 320,
            BufferedImage.TYPE_INT_RGB);
    Graphics2D g2 = badge.createGraphics();
    try {
      this.drawBadge(g2);
    } catch (IOException e) {
      return null;
    }
    return badge;
  }

  private void drawBadge(Graphics2D g2) throws IOException {
    // Set up graphics
    g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
            RenderingHints.VALUE_ANTIALIAS_ON);

    g2.setColor(new Color(255, 255, 255));
    g2.fillRect(0, 0, 512, 320);

    g2.setColor(new Color(25, 25, 25));
    g2.fillRect(0, 0, 512, 320);

    g2.setColor(new Color(255, 255, 255));
    g2.setFont(new Font("Source Sans Pro", Font.BOLD, 50));

    int nicknameWidth = g2.getFontMetrics().stringWidth(this.getCountryName());
    if (60 + 12 + nicknameWidth > 430) {
      g2.setFont(new Font("Source Sans Pro", Font.BOLD, 40));
      nicknameWidth = g2.getFontMetrics().stringWidth(this.getCountryName());
    }

    g2.drawString(this.getCountryName(), 45, 80);

    // Add the country flag
    if (60 + 12 + nicknameWidth < 430) {
      ClassLoader cl = this.getClass().getClassLoader();
      BufferedImage overlayImage;
      try {
        overlayImage = ImageIO
                .read(cl.getResource("cc/" + this.getCountry() + ".png"));
        g2.drawImage(overlayImage, 435, 45, null);
      } catch (IOException e) {
        // Not too upset if we can't load the flag
        e.printStackTrace();
      }
    }

    g2.setColor(new Color(94, 94, 94));
    g2.setFont(new Font("Source Code Pro", Font.PLAIN, 14));
    g2.drawString(this.getCountry(), 45, 110);

    g2.setColor(new Color(125, 70, 152));
    g2.setFont(new Font("Source Sans Pro", Font.BOLD, 12));
    g2.drawString("Advertised Bandwidth".toUpperCase(), 45, 175);
    g2.drawString("Relays".toUpperCase(), 300, 175);

    g2.setColor(new Color(255, 255, 255));
    g2.setFont(new Font("Source Sans Pro", Font.BOLD, 45));

    String formattedAdvertisedBandwidth = MicroblogUtils
            .formatBandwidth(this.getAdvertisedBandwidth());
    String formattedRelayCount = Integer.toString(this.getRelayCount());

    g2.drawString(formattedAdvertisedBandwidth.split(" ")[0], 45, 220);
    g2.drawString(formattedRelayCount, 300, 220);

    int bandwidthWidth = g2.getFontMetrics()
            .stringWidth(formattedAdvertisedBandwidth.split(" ")[0]);
    int relaysWidth = g2.getFontMetrics().stringWidth(formattedRelayCount);

    g2.setColor(new Color(255, 255, 255));
    g2.setFont(new Font("Source Sans Pro ExtraLight", Font.PLAIN, 45));
    g2.drawString(formattedAdvertisedBandwidth.split(" ")[1],
            55 + bandwidthWidth, 220);
    g2.drawString("relays", 310 + relaysWidth, 220);

    // Draw headings
    g2.setColor(new Color(125, 70, 152));
    g2.setFont(new Font("Source Sans Pro", Font.BOLD, 12));
    g2.drawString("Guard Probability".toUpperCase(), 45, 260);
    g2.drawString("Middle Probability".toUpperCase(), 215, 260);
    g2.drawString("Exit Probability".toUpperCase(), 385, 260);

    // Draw values
    g2.setColor(new Color(255, 255, 255));
    g2.setFont(new Font("Source Sans Pro Light", Font.PLAIN, 26));
    g2.drawString(String.format("%.5f", this.getGuardProbability()), 45, 290);
    g2.drawString(String.format("%.5f", this.getMiddleProbability()), 215, 290);
    g2.drawString(String.format("%.5f", this.getExitProbability()), 385, 290);
  }
}
