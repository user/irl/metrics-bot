package org.torproject.metrics.bot.tor;

import org.torproject.metrics.bot.MetricsBot;
import org.torproject.metrics.bot.microblog.BridgeStatusGenerator;
import org.torproject.metrics.bot.microblog.MicroblogStatus;
import org.torproject.metrics.bot.microblog.NetworkStatusGenerator;
import org.torproject.metrics.bot.microblog.RelayLocationStatusGenerator;
import org.torproject.metrics.bot.microblog.RelayStatusGenerator;

import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * A representation of the Tor network to query relay and bridge relay details.
 * Methods are provided for selecting relays and groups of relays at random.
 *
 * @author Iain R. Learmonth &lt;irl@torproject.org&gt;
 */
public class Network {
  GsonOnionoo onionoo;
  List<Relay> runningRelays;
  List<Bridge> runningBridges;

  public Network() {
    onionoo = new GsonOnionoo("https://onionoo.torproject.org/");
  }

  /**
   * Fetch and cache the latest details document from Onionoo containing all
   * currently running relays and bridges.
   */
  public void update() {
    int attempt = 0;
    while (true) {
      try {
        runningRelays = onionoo.getRelayDetails("running:true");
        runningBridges = onionoo.getBridgeDetails("running:true");
        break;
      } catch (OnionooNotUpdatedException e) {
        // TODO: Need to deal with this better
        if (attempt++ == 3) {
          MetricsBot.error(
                  "Help! My friend Onionoo is in trouble. I couldn't update.");
          System.exit(1);
        }
      }
    }
  }

  /**
   * Get a random relay from the Tor network.
   *
   * @return Details about the relay from the last Onionoo details document
   *         fetched.
   */
  public Relay getRandomRunningRelay() {
    SecureRandom secureRandom = new SecureRandom();
    return runningRelays.get(secureRandom.nextInt(runningRelays.size()));
  }

  /**
   * Get a random relay from the Tor network.
   *
   * @return Details about the relay from the last Onionoo details document
   *         fetched.
   */
  public Bridge getRandomRunningBridge() {
    SecureRandom secureRandom = new SecureRandom();
    return runningBridges.get(secureRandom.nextInt(runningBridges.size()));
  }

  /**
   * Get a random 2-letter country code that has at least one relay running in
   * the Tor network. This can be used as input to
   * {@link #getRunningRelaysForCountry(String)}.
   *
   * @return A 2-letter country code.
   * @see #getRunningRelaysForCountry()
   * @see #getRunningRelaysForCountry(String)
   */
  public String getRandomCountry() {
    Set<String> countries = new HashSet<String>();
    for (Relay relay : runningRelays) {
      if (relay.getCountry() != null) {
        countries.add(relay.getCountry());
      }
    }
    String[] countriesArray = countries.toArray(new String[countries.size()]);
    SecureRandom secureRandom = new SecureRandom();
    return countriesArray[secureRandom.nextInt(countriesArray.length)];
  }

  /**
   * Get a random AS number that has at least one relay running in the Tor
   * network. This can be used as input to
   * {@link #getRunningRelaysForAs(String)}.
   *
   * @return An AS number prefixed by "AS".
   * @see #getRunningRelaysForAs()
   * @see #getRunningRelaysForAs(String)
   */
  public String getRandomAs() {
    Set<String> asns = new HashSet<String>();
    for (Relay relay : runningRelays) {
      if (relay.getAsNumber() != null) {
        asns.add(relay.getAsNumber());
      }
    }
    String[] asnsArray = asns.toArray(new String[asns.size()]);
    SecureRandom secureRandom = new SecureRandom();
    return asnsArray[secureRandom.nextInt(asnsArray.length)];
  }

  /**
   * Returns a list of running relay details for a random AS. This is a wrapper
   * for {@link #getRunningRelaysForAs(String)}.
   *
   * @return A list of relay details.
   * @see #getRunningRelaysForAs(String)
   */
  public List<Relay> getRunningRelaysForAs() {
    return getRunningRelaysForAs(getRandomAs());
  }

  /**
   * Returns a list of running relays details for a chosen AS.
   *
   * @param as A string in the for "ASx" where x is an integer with no leading
   *           zeros.
   */
  public List<Relay> getRunningRelaysForAs(String as) {
    List<Relay> relaysForAs = new ArrayList<Relay>();
    for (Relay relay : runningRelays) {
      if (relay.getAsNumber() != null && relay.getAsNumber().equals(as)) {
        relaysForAs.add(relay);
      }
    }
    return relaysForAs;
  }

  /**
   * Returns a list of relay details for a random country. This is a wrapper for
   * {@link #getRunningRelaysForCountry(String)}.
   *
   * @return A list of relay details.
   * @see #getRunningRelaysForCountry(String)
   */
  public List<Relay> getRunningRelaysForCountry() {
    return getRunningRelaysForCountry(getRandomCountry());
  }

  /**
   * Returns a list of relay details for a chosen country.
   */
  public List<Relay> getRunningRelaysForCountry(String country) {
    List<Relay> relaysForCountry = new ArrayList<Relay>();
    for (Relay relay : runningRelays) {
      if (relay.getCountry() != null && relay.getCountry().equals(country)) {
        relaysForCountry.add(relay);
      }
    }
    return relaysForCountry;
  }

  /**
   * Returns a tweet using a random template with a random running relay or
   * group of running relays.
   *
   * <p> The following functions may be called to generate the tweets: </p>
   *
   * <ul> <li>{@link RelayStatusGenerator#generateAgeTweet(RelayImpl)}
   * <li>{@link RelayStatusGenerator#generateBandwidthTweet(RelayImpl)}
   * <li>{@link RelayLocationStatusGenerator#generateAsBandwidthTweet(List)}
   * <li>{@link
   * RelayLocationStatusGenerator#generateCountryBandwidthTweet(List)}
   * <li>{@link BridgeStatusGenerator#generateAgeTweet(Bridge)}
   * <li>{@link BridgeStatusGenerator#generateBandwidthTweet(Bridge)} </ul>
   *
   * @return A formatted Tweet.
   * @throws OnionooMissingInformationException
   *           when information required to generate the tweet is missing from
   *           the RelayDetails object (probably because Onionoo did not have
   *           it)
   */
  public MicroblogStatus getNetworkTweet()
          throws OnionooMissingInformationException {
    return NetworkStatusGenerator.generateMapTweet(getRunningRelays());
  }

  /**
   * Returns a tweet using a random template with a random running relay or
   * group of running relays.
   *
   * <p> The following functions may be called to generate the tweets: </p>
   *
   * <ul> <li>{@link RelayStatusGenerator#generateAgeTweet(RelayImpl)}
   * <li>{@link RelayStatusGenerator#generateBandwidthTweet(RelayImpl)}
   * <li>{@link RelayLocationStatusGenerator#generateAsBandwidthTweet(List)}
   * <li>{@link
   * RelayLocationStatusGenerator#generateCountryBandwidthTweet(List)}
   * <li>{@link BridgeStatusGenerator#generateAgeTweet(Bridge)}
   * <li>{@link BridgeStatusGenerator#generateBandwidthTweet(Bridge)} </ul>
   *
   * @return A formatted Tweet.
   * @throws OnionooMissingInformationException
   *           when information required to generate the tweet is missing from
   *           the RelayDetails object (probably because Onionoo did not have
   *           it)
   */
  public MicroblogStatus getRelayTweet()
          throws OnionooMissingInformationException {
    Integer attempts = 0;
    MicroblogStatus tweet = null;
    while (tweet == null) {
      try {
        tweet = attemptRandomTweet();
      } catch (OnionooMissingInformationException e) {
        ++attempts;
        if (attempts > 10) {
          // it's really not happening
          throw new OnionooMissingInformationException();
        }
      }
    }
    return tweet;
  }

  private MicroblogStatus attemptRandomTweet()
          throws OnionooMissingInformationException {
    SecureRandom secureRandom = new SecureRandom();
    Integer tweetType = secureRandom.nextInt(2);

    switch (tweetType) {
      //case 0:
      //  return RelayStatusGenerator.generateAgeTweet(getRandomRunningRelay());
      //case 1:
      //  return RelayStatusGenerator
      //          .generateBandwidthTweet(getRandomRunningRelay());
      case 0:
        return RelayLocationStatusGenerator
                .generateAsBandwidthTweet(getRunningRelaysForAs());
      case 1:
        return RelayLocationStatusGenerator
                .generateCountryBandwidthTweet(getRunningRelaysForCountry());
      //case 4:
      //  return BridgeStatusGenerator
      //          .generateBandwidthTweet(getRandomRunningBridge());
      //case 5:
      //  return BridgeStatusGenerator
      //          .generateBandwidthTweet(getRandomRunningBridge());
      default:
        // this will never actually fall through, unless there's a bug in
        // SecureRandom...
        return null;
    }
  }

  public List<Relay> getRunningRelays() {
    return runningRelays;
  }

  public List<Bridge> getRunningBridges() {
    return runningBridges;
  }
}
