package org.torproject.metrics.bot.tor;

import java.util.Arrays;
import java.util.List;

class GsonOnionooDetails {
  GsonRelay[] relays;
  GsonBridge[] bridges;

  public List<Relay> getRelays() {
    return Arrays.asList(relays);
  }

  public List<Bridge> getBridges() {
    return Arrays.asList(bridges);
  }
}