package org.torproject.metrics.bot.tor;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonParseException;

import java.lang.reflect.Type;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

class ZonedDateTimeDeserializer implements JsonDeserializer<ZonedDateTime> {
  @Override
  public ZonedDateTime deserialize(JsonElement json, Type typeOfT,
          JsonDeserializationContext context) throws JsonParseException {
    DateTimeFormatter formatter =
            DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
    LocalDateTime ldt = LocalDateTime.parse(json.getAsString(), formatter);
    ZonedDateTime zdt = ldt.atZone(ZoneId.of("UTC"));
    return zdt;
  }
}