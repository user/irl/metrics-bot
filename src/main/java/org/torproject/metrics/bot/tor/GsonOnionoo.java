package org.torproject.metrics.bot.tor;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.fluent.Request;

import java.io.IOException;
import java.time.ZonedDateTime;
import java.util.List;

/**
 * An Onionoo client implementation using the Gson parser. This is not intended
 * to be a full implementation of the Onionoo protocol, just enough to satisfy
 * the needs of MetricsBot.
 * 
 * @author Iain R. Learmonth &lt;irl@torproject.org&gt;
 */
class GsonOnionoo {

  String baseUrl;

  /**
   * Create a new Onionoo client.
   * 
   * @param baseUrl
   *          the base URL to use, for example "https://onionoo.torproject.org/"
   *          (this should include the trailing slash)"
   */
  public GsonOnionoo(String baseUrl) {
    this.baseUrl = baseUrl;
  }

  /**
   * Fetch a list of all known relays matching an Onionoo search query.
   * 
   * @return a list of all relays currently known to Onionoo (this does not
   *         include bridge relays)
   * @throws OnionooNotUpdatedException
   *           Thrown if it was not possible to fetch a list of relays.
   */
  public List<Relay> getRelayDetails(String search)
          throws OnionooNotUpdatedException {
    GsonOnionooDetails details = getDetailsDocument("relay", search);
    List<Relay> relaysList = details.getRelays();
    if (relaysList.isEmpty()) {
      throw new OnionooNotUpdatedException();
    }
    return relaysList;
  }

  public List<Bridge> getBridgeDetails(String search)
          throws OnionooNotUpdatedException {
    GsonOnionooDetails details = getDetailsDocument("bridge", search);
    List<Bridge> relaysList = details.getBridges();
    if (relaysList.isEmpty()) {
      throw new OnionooNotUpdatedException();
    }
    return relaysList;
  }

  private GsonOnionooDetails getDetailsDocument(String type, String search)
          throws OnionooNotUpdatedException {
    String detailsJson;
    if (search != null) {
      search = "&search=" + search;
    } else {
      search = "";
    }
    try {
      detailsJson = this.fetch("details?type=" + type + search);
    } catch (IOException e) {
      throw new OnionooNotUpdatedException();
    }
    GsonBuilder gsonBuilder = new GsonBuilder();
    gsonBuilder.setFieldNamingPolicy(
            FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES);
    gsonBuilder.registerTypeAdapter(ZonedDateTime.class,
            new ZonedDateTimeDeserializer());
    Gson gson = gsonBuilder.create();
    return gson.fromJson(detailsJson, GsonOnionooDetails.class);
  }

  private String fetch(String url) throws ClientProtocolException, IOException {
    return Request.Get(this.baseUrl + url).execute().returnContent().asString();
  }
}