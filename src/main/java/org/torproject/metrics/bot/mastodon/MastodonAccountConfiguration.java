package org.torproject.metrics.bot.mastodon;

import org.torproject.metrics.bot.microblog.MicroblogAccountConfiguration;
import org.torproject.metrics.bot.microblog.StatusType;

import java.util.List;

public class MastodonAccountConfiguration
        implements MicroblogAccountConfiguration {

  private String friendlyName;
  private String instanceHost;
  private String accessToken;
  private List<StatusType> statusTypes;

  @SuppressWarnings("checkstyle:javadocmethod")
  public MastodonAccountConfiguration(String friendlyName,
          List<StatusType> statusTypes, String instanceHost,
          String accessToken) {
    this.friendlyName = friendlyName;
    this.instanceHost = instanceHost;
    this.accessToken = accessToken;
    this.statusTypes = statusTypes;
  }

  public String getFriendlyName() {
    return friendlyName;
  }

  public String getInstanceHost() {
    return instanceHost;
  }

  public String getAccessToken() {
    return accessToken;
  }

  public List<StatusType> getStatusTypes() {
    return statusTypes;
  }

  @Override
  public String toString() {
    return "<MastodonAccountConfiguration name=" + friendlyName
            + ", instanceHost=" + instanceHost + ", accessToken=" + accessToken
            + ">";
  }

}
